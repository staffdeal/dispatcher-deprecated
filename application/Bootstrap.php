<?php

use Poundation\PMailAddress;

use Poundation\PURL;

//use Epr\Epr_Obj\Epr_Obj;

use Poundation\PString;
use Poundation\Server\PRequest;

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap {
	
	protected function _bootstrap($resource = null) {
		try {
			parent::_bootstrap($resource);
		} catch (Exception $e) {
			parent::_bootstrap('frontController');
			$front = $this->getResource('frontController');
			$front->registerPlugin(new Plugins_BootstrapError($e));
		}
	}
	
	protected function _initAirbrake() {
		require_once('Airbrake/EventHandler.php');
		Airbrake\EventHandler::start('08ebfa4b-3d5a-1a75-21d3-3f7e96e38771', false, array(
			'apiEndPoint' => 'https://exceptions.codebasehq.com/notifier_api/v2/notices'
					));
	}

    protected function _initPlugins()
    {
        $front = Zend_Controller_Front::getInstance();
        $front->registerPlugin(new Epr_Controller_Plugin_ErrorHandler());
        $front->registerPlugin(new Epr_Controller_Plugin_ErrorControllerSwitcher());
    }
		
	/**
	 * Initializing the Config
	 * @return type 
	 */
	protected function _initConfig() {
		$config = $this->getOptions();
		Zend_Registry::set('config', $config);
		return $config;
	}
	
	/**
	 * Initializing the Error Handling
	 */
	protected function _initErrorHandling() {

		$config = $this->getOptions();

		error_reporting(E_ALL | E_STRICT);
		ini_set('display_errors', $config['phpSettings']['display_errors']);

	}

	/**
	 * Setting up Logging
	 */
	protected function _initLogging() {

		$logger = new Epr_Log();

		/** get the config */
		$config = $this->getOptions();

		$loglevel = (int) $config['logging']['level'];
		$filelogging = (bool) $config['logging']['writer']['file'];
		$firebuglogging = (bool) $config['logging']['writer']['firebug'];

		$filter = new Zend_Log_Filter_Priority($loglevel);

		$format = '%timestamp% ' . uniqid() . ' %priorityName%: %message% ' . PHP_EOL;

		$formatter = new Zend_Log_Formatter_Simple($format);

		if ($filelogging) {
			//writes to path
			$writer = new Zend_Log_Writer_Stream(ROOT_PATH . '/logs/' . $config['project']['name'] . '.log');
			$writer->addFilter($filter);
			$writer->setFormatter($formatter);
			$logger->addWriter($writer);
		}

		if ($firebuglogging) {
			$writer_fb = new Zend_Log_Writer_Firebug();
			$writer_fb->addFilter($filter);
			$logger->addWriter($writer_fb);

		}

		if ($filelogging || $firebuglogging) {
			Zend_Registry::set('logger', $logger);

		} else {
			$writer_null = new Zend_Log_Writer_Null();
			$logger->addWriter($writer_null);
			Zend_Registry::set('logger', $logger);
		}

		_logger()->info("Bootstrap: Logger startet");
	}
	
	protected function _initDispatcherConfig() {
		$config = Zend_Registry::get('config');
		if (isset($config['dispatcher']['configFile'])) {
			$dispatcherConfigFile = $config['dispatcher']['configFile'];
			if (file_exists($dispatcherConfigFile)) {
				$dispatcherConfig = new Zend_Config_Ini($dispatcherConfigFile);
				Zend_Registry::set('dispatcher', $dispatcherConfig);
	
				// further checks
				if (!isset($dispatcherConfig->database->salt)) {
					throw new Exception('No salt specified in dispatcher.ini');
				}
	
				if (!isset($dispatcherConfig->web->url)) {
					throw new Exception('No web url specified in dispatcher.ini');
				}
		
			} else {
				throw new Exception('Cannot open disptacher config file ' . $dispatcherConfigFile . '.');
			}
		} else {
			throw new Exception('No dispatcher config file specified in application.ini.');
		}
	}
	
	protected function _initSecurity() {
	
		$config = Zend_Registry::get('dispatcher');
		if (isset($config->security->salt)) {
				
			$salt = __($config->security->salt);
			if ($salt->length() < 10) {
				throw new Exception('Salt too short, must be at least 10 charachters long.');
			} else {
				Zend_Registry::set('salt', (string)$salt);
			}
				
		} else {
			throw new Exception("No salt found in client's config. Add salt to section 'security'.");
		}
	}
	
	/**
	 * Setting up Mail
	 */
	protected function _initMail() {

		$config = Zend_Registry::get('dispatcher');
		
		$mailConfig = array(
				'auth' => $config->mail->authType,
				'username' => $config->mail->username,
				'password' => $config->mail->password);
		
		$transport = new Zend_Mail_Transport_Smtp($config->mail->host, $mailConfig);
		
		Zend_Mail::setDefaultTransport($transport);
		Zend_Registry::set('mailTransport',$transport);

		_logger()->info("Bootstrap: Mail Setup finished");

	}

	protected function _initAcl() {
		$acl = Plugins_Auth_Acl::getInstance();
		Zend_View_Helper_Navigation_HelperAbstract::setDefaultAcl($acl);
		Zend_View_Helper_Navigation_HelperAbstract::setDefaultRole('guest');
		Zend_Registry::set('Zend_Acl', $acl);
		_logger()->info("Bootstrap: ACL initialized");
		return $acl;
	}

	/**
	 * Routes Configuration
	 */
	protected function _initRouting() {

		$frontController = Zend_Controller_Front::getInstance();

		$router = $frontController->getRouter();

		$route = new Zend_Controller_Router_Route(':module/:controller/:action/*',
				array('language' => 'en', 'module' => 'default', 'controller' => 'index', 'action' => 'index'));
		$router->addRoute('lang_default', $route);


        /*
         * Adding File Handling Route
         */
        $filesRoute = new Zend_Controller_Router_Route('files/:type/:id/:file',array('module'=>'default', 'controller'=>'index', 'action'=>'file'));
        $router->addRoute('files', $filesRoute);
        $filesRoute2 = new Zend_Controller_Router_Route('files/:type/:id',array('module'=>'default', 'controller'=>'index', 'action'=>'file'));
        $router->addRoute('files2', $filesRoute2);
        
        
		$auth = Zend_Auth::getInstance();
		$frontController->registerPlugin(new Plugins_Auth_AccessControl($auth));

		$errorHandling = new Zend_Controller_Plugin_ErrorHandler();
		$errorHandling->setErrorHandler(array('module' => 'default', 'controller' => 'error', 'action' => 'error'));

		$frontController->registerPlugin($errorHandling);

		$frontController->setParam('useDefaultControllerAlways', true);

	}

	protected function _initTranslation() {
		$translate = new Zend_Translate(array('adapter' => 'array', 'content' => ROOT_PATH . '/languages/de.php', 'locale' => 'de'));
		$translate->addTranslation(array('adapter' => 'array', 'content' => ROOT_PATH . '/languages/en.php', 'locale' => 'en'));
		Zend_Registry::set('Zend_Translate', $translate);

	}
	protected function _initDoctrine() {

		$config = Zend_Registry::get('config');
		
		$commonLibPath = ROOT_PATH . '/../Common';
		if (isset($config['paths'])) {
			$paths = $config['paths'];
			if (isset($paths['common'])) {
				$commonLibPath = $paths['common'];
			}
		}
		if (!file_exists($commonLibPath . '/Doctrine')) {
			throw new Exception('Cannot locate Doctrine, specify paths.common in application.ini');
		} else {
			Zend_Registry::set('commonLibPath', $commonLibPath);
		}
		
		$loader = new \Doctrine\Common\ClassLoader($commonLibPath);
		$loader->register();			
		$annotationNs = 'Doctrine\\ODM\\CouchDB\\Mapping\\Annotations';
		
		$path = $commonLibPath;
		Doctrine\Common\Annotations\AnnotationRegistry::registerAutoloadNamespace($annotationNs, $path);
	}

	protected function _initDatabase() {
		
		$config = Zend_Registry::get('dispatcher');

		$databaseHost = __($config->database->host);
		if ($databaseHost->length() == 0) {
			_logger()->warn("No database host specified in config, using 'localhost'.");
			$databaseHost = __('localhost');
		}
		
		$databaseUser = __($config->database->username);
		$databasePassword = __($config->database->password);
		
		$databasePort = __($config->database->port);
		if ($databasePort->length() == 0) {
			_logger()->warn("No database port specified in config, using '5984'.");
			$databasePort = __('5984');
		}
		
		$databaseName = __($config->database->dbname)->lowercase();
		if ($databaseName->length() == 0) {
			throw new Exception("No database name found in dispatcher's config.");
		}
		
		$httpClient = new \Doctrine\CouchDB\HTTP\SocketClient($databaseHost, $databasePort->integerValue(), (string)$databaseUser, (string)$databasePassword);

		$dbClient = new Doctrine\CouchDB\CouchDBClient($httpClient, $databaseName);

		$config = new \Doctrine\ODM\CouchDB\Configuration();
		$metadataDriver = $config->newDefaultAnnotationDriver();

		$config->setProxyDir(ROOT_PATH . "/proxies");
		$config->setMetadataDriverImpl($metadataDriver);
		$config->setLuceneHandlerName('_fti');

		$dm = new \Doctrine\ODM\CouchDB\DocumentManager($dbClient, $config);

		Zend_Registry::set('dm', $dm);
		
		try {
			// create database if not existing
			$httpClient->request('PUT', '/' . $databaseName);
		} catch (Exception $e) {
			throw new Exception('Connection to Database failed');
		}

	}
	
	protected function _initApplication() {
	
		$application = Epr_Application::application();
		Zend_Registry::set('app', $application);
		_logger()->info('Application running using document version ' . $application->getDocumentsVersion());
	}
		
	protected function _initFirstAdministrator() {


 		if (!Epr_User_Collection::hasActiveAdministrator()) {

			$firstAdministrator = new Epr_User('daniel@keksbox.com');
			$firstAdministrator->setPassword('admin');
			$firstAdministrator->setRole(Epr_Roles::ROLE_ADMIN);
			$firstAdministrator->setFirstname('Administrator');
			$firstAdministrator->activate();
			_dm()->persist($firstAdministrator);
			_dm()->flush();
		}
	}
	
	protected function _initCaching() {
		_logger()->info("Bootstrap: Cacheing startet but not working");
	}
	
}

