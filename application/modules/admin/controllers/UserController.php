<?php

/**
 * Index Controller for admin module
 *
 */
class Admin_UserController extends Epr_Backend_Controller_Action
{

    public function indexAction()
    {
        $this->_redirect($this->getHelper('CustomUrl')->custom('list','user', 'admin'));
    }

    public function listAction()
    {
        $this->view->assign('pageHeading', $this->view->translate('User List'));

        $users = _dm()->getRepository('Epr_User')->findAll();

        $this->view->assign('editUrl', $this->getHelper('CustomUrl')->custom('update','user', 'admin'));
        $this->view->assign('deleteUrl', $this->getHelper('CustomUrl')->custom('delete','user', 'admin'));
        $this->view->assign('deactivateUrl', $this->getHelper('CustomUrl')->custom('deactivate','user', 'admin'));

        $this->view->assign('users', $users);

    }

    public function deleteAction()
    {
        $id = $this->_request->getParam('id', false);
        $user = _dm()->getRepository('Epr_User')->find($id);
        if($user instanceof Epr_User){
            _dm()->remove($user);
            $this->getHelper('sysFlashMessenger')->success('User removed.');
            $this->_redirect($this->getHelper('CustomUrl')->custom('list','user','admin'));
        }

        $this->getHelper('sysFlashMessenger')->error('Given ID is no User.');
        $this->_redirect($this->getHelper('CustomUrl')->custom('list','user','admin'));

    }

    public function deactivateAction()
    {
        $id = $this->_request->getParam('id', false);
        $user = _dm()->getRepository('Epr_User')->find($id);
        if($user instanceof Epr_User){
            $user->setActive($user->isActive()?false:true);
            $user->save();
            $this->getHelper('sysFlashMessenger')->success('User deactivated.');
            $this->_redirect($this->getHelper('CustomUrl')->custom('list','user','admin'));
        }

        $this->getHelper('sysFlashMessenger')->error('Given ID is no User.');
        $this->_redirect($this->getHelper('CustomUrl')->custom('list','user','admin'));
    }

    public function updateAction()
    {
        $this->view->assign('pageHeading', $this->view->translate('Add User'));

        $request = $this->getRequest();

        $id = $request->getParam('id', false);

        $user = null;

        if($id){
            $user = _dm()->getRepository('Epr_User')->find($id);
        }
        $form = Epr_Form_User::userForm($user);

        if($request->isPost() && $form->isValid($request->getParams()))
        {
            $userToSave = null;
            // Save user
            if($id){
                $userToSave = _dm()->getRepository('Epr_User')->find($id);
            }else{
                $userToSave = new Epr_User($request->getParam('email'));
            }
            $userToSave->setEmail($request->getParam('email'));
            $userToSave->setLastname($request->getParam('lastname'));
            $userToSave->setFirstname($request->getParam('firstname'));
            $userToSave->setRole($request->getParam('role'));
            $userToSave->setActive($request->getParam('isActive'));

            $password = $request->getParam('password', false);
            $passwordRepeat = $request->getParam('password_repeat', false);

            if($password && $passwordRepeat && $password == $passwordRepeat){
                $userToSave->setPassword($password);
            }

            try{
                $userToSave->save();
                $form->reset();
                $this->getHelper('sysFlashMessenger')->success('User saved.');
                $this->_redirect($this->getHelper('CustomUrl')->custom('list', 'user', 'admin'));
            }catch(Exception $e){
                $this->getHelper('sysFlashMessenger')->error($this->view->translate('Item could not be saved'));
                $this->getHelper('sysFlashMessenger')->info($e->getMessage());
            }

        }

        $this->view->assign('form', $form);

    }


}
