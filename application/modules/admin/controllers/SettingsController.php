<?php

/**
 * Index Controller for admin module
 *
 */
class Admin_SettingsController extends Epr_Backend_Controller_Action
{

    public function indexAction()
    {
        $this->view->assign('pageHeading', $this->view->translate('Settings'));
    }

    public function generalAction() {
    	$this->view->assign('pageHeading', $this->view->translate('General Settings'));
    }
    
    public function modulesAction() {
    	$this->view->assign('pageHeading', $this->view->translate('Module Settings'));
    	$modules = _app()->getModules();
    	$this->view->assign('modules',$modules);
    	
    	$this->view->assign('editUrl',$this->getHelper('CustomUrl')->custom('updateModule','settings', 'admin'));
    	
    }
    
    public function updatemoduleAction() {
    	$this->view->assign('pageHeading', $this->view->translate('Edit Module Settings'));
    	
    	$allModules = _app()->getModules();
    	
    	$id = $this->_request->getParam('id', false);
    	if ($id !== false) {
    		
    		$module = _app()->getModuleWithIdentifier($id);
    		if ($module) {
    			$form = Epr_Form_Settings::moduleForm($module);
    			    			
    			if ($form !== false) {
    			
    				$moduleFields = $module->getSettingsFormFields();
    				if (is_array($moduleFields) && count($moduleFields) > 0) {
    					$form->addElements($moduleFields);
    				}
    				
    				if ($this->_request->isPost() && $form->isValid($this->_request->getParams())) {
    						
    					foreach($moduleFields as $field) {
    						if ($field instanceof Zend_Form_Element) {
    							$module->saveSettingsField($field);
    						}
    					}
    					 
    					try {
    						_dm()->flush($module);
    						
    						if (!_isDebugging()) {
    							$this->_redirect($this->getHelper('CustomUrl')->custom('modules','settings','admin'));
    						}
    					} catch (Exception $e) {
    						$this->getHelper('sysFlashMessenger')->error($this->view->translate('Module could not be saved'));
    						$this->getHelper('sysFlashMessenger')->info($e->getMessage());
    					}
    				}
    				 
    				if (count($form->getElements()) > 0) {
    				
    					$submit = new Zend_Form_Element_Submit('submit');
    					$submit->setLabel('');
    					$submit->setValue('Submit');
    					$submit->setAttrib('class','form-submit');
    					$submit->setDecorators(array('Composite'));
    					
    					$reset = new Zend_Form_Element_Submit('reset');
    					$reset->setValue('Reset');
    					$reset->setLabel('');
    					$reset->setAttrib('class','form-reset');
    					$reset->setDecorators(array('Composite'));
    					
    					$form->addElements(array($submit,$reset));
    					
    				} else {
    					$form = $this->view->translate('There are no settings for this module.');
    				}
    				
    			}
    			
    			$this->view->assign('form',$form);
    		}
    		
    	}
    }
}
