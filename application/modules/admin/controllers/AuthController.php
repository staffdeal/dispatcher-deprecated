<?php

/**
 * Index Controller for admin module
 *
 */
class Admin_AuthController extends Zend_Controller_Action
{

    public function loginAction()
    {
 
        $this->_helper->layout()->setLayout('login');

        $loginForm  = Epr_Form_User::getLoginForm();
        $forgetForm = Epr_Form_User::getForgetPasswordForm();
        $errorMessage = null;

        $request = $this->getRequest();
        if($request->isPost()){
            $submitTask = $request->getParam('submit', false);
            switch($submitTask)
            {
                case 'login':
                    if($loginForm->isValid($request->getParams()))
                    {
                        _logger()->info('is Valid');
                        $authAdapter = new Epr_Auth_Adapter();
                        $authAdapter->setIdentity($request->getParam('email'));
                        $authAdapter->setCredential($request->getParam('password'));

                        $auth = Zend_Auth::getInstance();
                        if($auth->authenticate($authAdapter)->isValid())
                        {
                            /** @var $identity Epr_User */
                            $identity = $auth->getIdentity();
                            $identity->setAsAuthenticated(true);
                            Epr_System::setUser($identity);


                            $this->_redirect($this->getHelper('CustomUrl')->custom('index','index','admin'));
                        }else{

                            $errorMessage = "Login Failed";

                        }
                    }
                    break;
                case 'forgot':
                    if($forgetForm->isValid($request->getParams()))
                    {

                    }
                    break;
            }
        }

        $this->view->assign('error', $errorMessage);
        $this->view->assign('loginForm', $loginForm);
        $this->view->assign('forgetForm', $forgetForm);

    }

    public function logoutAction() {
        _logger()->info('logout');
        $this->getHelper('viewRenderer')->setNoRender();

        $request = $this->getRequest();
        $auth = Zend_Auth::getInstance();
        $auth->clearIdentity();
        Epr_System::setUser(new Epr_User());
        $this->_redirect($this->getHelper('CustomUrl')->custom('login','auth','admin'));
    }
    
}
