<?php

/**
 * Index Controller for admin module
 *
 */

//use DateTime;
class Admin_DealsController extends Epr_Backend_Controller_Action
{
    public function indexAction()
    {

        $this->_redirect($this->getHelper('CustomUrl')->custom('list','deals','admin'));

    }

    public function listAction()
    {
        $this->view->assign('pageHeading', $this->view->translate('Deals List'));

        $deals = _dm()->getRepository('Epr_Deal')->findAll();
        $this->view->assign('editUrl', $this->getHelper('CustomUrl')->custom('update','deals', 'admin'));
        $this->view->assign('deleteUrl', $this->getHelper('CustomUrl')->custom('delete-deal','deals', 'admin'));
        $this->view->assign('deactivateUrl', $this->getHelper('CustomUrl')->custom('activate-deal','deals', 'admin'));

        $this->view->assign('deals', $deals);
    }

    public function updateAction()
    {
        $this->view->assign('pageHeading', $this->view->translate('Add Deal'));

        $id = $this->_request->getParam('id', false);

        $deal = null;

        if ($id) {
            $deal = _dm()->getRepository('Epr_Deal')->find($id);
        }


        $form = Epr_Form_Deal::dealForm($deal);

        if ($this->_request->isPost() && $form->isValid($this->_request->getParams())) {
            if (is_null($deal)) {
                $deal = new Epr_Deal();
            }
            $uploadPath = "/tmp/";
            /* Uploading Document File on Server */

            $upload = new Zend_File_Transfer_Adapter_Http();
            $upload->setDestination($uploadPath);

            $options = array('ignoreNoFile' => TRUE);
            $upload->setOptions($options);


            if (!$upload->receive()) {
                $messages = $upload->getMessages();
                $this->getHelper('sysFlashMessenger')->notice(implode("\n", $messages));
            }

            try {
                // upload received file(s)

//                _logger()->info(print_r($upload->getFileName('titleImage'), true));

                $name = $upload->getFileName('titleImage', false);
                $mimeType = Epr_System::detect_mime($name);

                if($mimeType){

                    $fh = fopen($uploadPath.$name, 'r');
                    $content = fread($fh, filesize($uploadPath.$name));

                    $deal->attachments['image'] = \Doctrine\CouchDB\Attachment::createFromBinaryData($content, $mimeType);

                    unlink($uploadPath.$name);
                }


            } catch (Zend_File_Transfer_Exception $e) {
                $this->getHelper('sysFlashMessenger')->error($e->getMessage());
            }


            $deal->setTitle($this->_request->getParam('title'));
            $deal->setStartDate(DateTime::createFromFormat('Y-m-d', $this->_request->getParam('startdate')));
            $deal->setEndDate(DateTime::createFromFormat('Y-m-d', $this->_request->getParam('enddate')));
            $deal->setText($this->_request->getParam('text'));
            $deal->setTeaser($this->_request->getParam('teaser'));
            $deal->setAsFeatured($this->_request->getParam('isFeatured'));
            $deal->setActive($this->_request->getParam('isActive'));

            $removeImage = $this->_request->getParam('removeImage', false);
            if($removeImage !== false && $removeImage != 0 ){
                unset($deal->attachments['image']);
            }

            try {
                $deal->save();
                $form->reset();
                $this->getHelper('sysFlashMessenger')->success('Item saved.');
                $this->_redirect($this->getHelper('CustomUrl')->custom('list','deals','admin'));
            } catch(Exception $e) {
                $this->getHelper('sysFlashMessenger')->error($this->view->translate('Item could not be saved'));
                $this->getHelper('sysFlashMessenger')->info($e->getMessage());
            }
        }

        $this->view->assign('form', $form);

    }

    public function activateDealAction()
    {
        $id = $this->_request->getParam('id', false);

        $deal = _dm()->getRepository('Epr_Deal')->find($id);
        if ($deal instanceof Epr_Deal) {
            $active = $deal->isActive() ? false : true;
            $deal->setActive($active);
            $deal->save();

            $this->getHelper('sysFlashMessenger')->setStatic()->success(!$active ? 'Item deactivated' : 'Item activated.');
            $this->_redirect($this->getHelper('CustomUrl')->custom('list', 'deals', 'admin'));
        }

        $this->getHelper('sysFlashMessenger')->setStatic()->error('Updating Item failed');
        $this->_redirect($this->getHelper('CustomUrl')->custom('list', 'deals', 'admin'));
    }

    public function deleteDealAction()
    {
        $id = $this->_request->getParam('id', false);
        $deal = _dm()->getRepository('Epr_Deal')->find($id);
        if ($deal instanceof Epr_Deal) {
            _dm()->remove($deal);
            $this->getHelper('sysFlashMessenger')->setStatic()->success('Item removed.');
            $this->_redirect($this->getHelper('CustomUrl')->custom('list', 'deals', 'admin'));
        }

        $this->getHelper('sysFlashMessenger')->setStatic()->error('Given ID is no Deal.');
        $this->_redirect($this->getHelper('CustomUrl')->custom('list', 'deals', 'admin'));
    }

}
