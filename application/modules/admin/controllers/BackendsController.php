<?php

use Airbrake\Exception;

class Admin_BackendsController extends Epr_Backend_Controller_Action {

	public function indexAction() {
		
	}
	
	public function listAction() {
		$this->view->assign('t','Test');
	}
	
	public function registerAction() {

		$form = Epr_Form_Backends::URLForm();

		if ($this->_request->isPost() && $form->isValid($this->_request->getParams())) {
			
			$url = $form->getElement('url')->getValue();
			
			$message = Epr_Com_Message::createMessageWithName('pair');
			$message->setValue((string)_app()->getPublicURL(), 'url');
			$message->setValue(_app()->getVersion(), 'version');
			
			$connection = Epr_Com_Connection::createOutboundConnection($url, $message->sign(''));
			$response = $connection->start();
			
			$message = false;
			
			if ($response->getStatus() === Epr_Com_Connection_Response::STATUS_HOSTNOTFOUND) {
				$message = 'The host could not be found.';
			} else if ($response->getStatus() === Epr_Com_Connection_Response::STATUS_SUCCESS) {

                $receivedMessage = $response->getReceivedMessage();
                $backendName = $receivedMessage->getValue('name');

                $backend = new Epr_Backend();
                $backend->

			} else {
				$message = 'Something went wrong, please contact the administrator.';
			}
		}
		
		$this->view->assign('form',$form);
		
		if ($message !== false) {
			$this->getHelper('sysFlashMessenger')->setStatic()->info($message);
		}
		
		
	}
	
}

?>