<?php

/**
 * Index Controller for admin module
 *
 */
class Epr_Backend_Controller_Action extends Epr_Controller_Action
{

    protected $_flashMessenger = null;


    public function preDispatch()
    {
        parent::preDispatch();
        $this->_setLanguage();

        $this->_getTopSearch();
        $this->_getNavigation();
        $this->_getNavRight();

        $this->_helper->layout()->setLayout('admin');
    }


    private function _setLanguage()
    {
        $language = $this->getRequest()->getParam('language', 'de');

        switch($language) {
            case "de":
                $locale = 'de_DE'; break;
            case "en":
                $locale = 'en_US'; break;
            default:
                $locale = "de_DE"; break;
        }

        $zl = new Zend_Locale();
        $zl->setLocale($locale);
        Zend_Registry::set("Zend_Locale", $zl);
        _logger()->info('Current Language = "'.$language.'"');
    }

    private function _getTopSearch()
    {
        $view = new Zend_View();
        $view->setBasePath(APPLICATION_PATH.'modules/admin/views/placeholder/');

        $this->view->placeholder('top-search')->append($view->render('top-search.phtml'));

    }

    private function _getNavigation()
    {
        $view = new Zend_View();
        $view->setBasePath(APPLICATION_PATH.'modules/admin/views/placeholder/');
        $view->assign('controller',$this->getRequest()->getControllerName());
        $view->assign('action',$this->getRequest()->getActionName());

        $this->view->placeholder('navigation')->append($view->render('navigation.phtml'));
    }

    private function _getNavRight()
    {
        $view = new Zend_View();
        $view->setBasePath(APPLICATION_PATH.'modules/admin/views/placeholder/');

        $this->view->placeholder('navigation-right')->append($view->render('nav-right.phtml'));
    }

}
