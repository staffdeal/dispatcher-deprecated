<?php
/**
 * Builds the setup for an Erp_Backend_Form
 * User: janfanslau
 * Date: 07.09.12
 * Time: 18:34
 */

class Epr_Form extends Zend_Form
{

    public $elementDecorators = array(
        'ViewHelper',
        'Description',
        'Errors',
        array(array('td' => 'HtmlTag'), array('tag' => 'td')),
        array(array('td' => 'HtmlTag'), array('tag' => 'td')),
        array('Label', array('tag' => 'th', 'class'=>'test')),
        array(array('row' => 'HtmlTag'), array('tag' => 'tr')),
        );

    public function init($options = null)
    {


        $this->addElementPrefixPath('Epr_Form_Element_Decorator',
            'Epr/Form/Element/Decorator',
            'decorator');



        $this->setDecorators(array(
            'FormElements',
            array('HtmlTag', array('tag' => 'table', 'border'=>'0', 'cellspacing'=>'0', 'id'=>'id-form')),
            'Form',
        ));

        $this->setMethod('post');

    }

    public function loadDefaultDecorators()
    {
//        $this->setDecorators(array(
//            'FormElements',
//            'Form'
//        ));
    }

}
