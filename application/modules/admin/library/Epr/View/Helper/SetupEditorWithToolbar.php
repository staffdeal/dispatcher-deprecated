<?php
class Epr_View_Helper_SetupEditorWithToolbar extends Zend_View_Helper_Abstract{



    function setupEditorWithToolbar($textareaIdArray, $toolbar){
        $ctrl = Zend_Controller_Front::getInstance();
        $this->view->headScript()->appendFile( $ctrl->getBaseUrl().'/js/ckeditor/ckeditor.js');

        $script = '';
        foreach($textareaIdArray as $textareaId)
            $script .= 'var editor = CKEDITOR.replace("' . $textareaId . '", { toolbar : "' .$toolbar. '", external_image_list_url : "myexternallist.js" });';
        return $script . '';
    }
}