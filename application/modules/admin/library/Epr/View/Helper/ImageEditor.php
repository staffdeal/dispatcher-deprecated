<?php
/**
 * meinERP
 * User: Jan Fanslau
 * Date: 27.10.12
 * Time: 15:51
 */
class Epr_View_Helper_ImageEditor extends Zend_View_Helper_Abstract
{

    protected $partial = 'scripts/imageEditor.phtml';

    private $sortScript;

    private $removeScript;

    private $images;

    private $allowSorting;

    private $allowRemoving;

    private $itemId;

    function imageEditor($options)
    {
        $this->prepareOptions($options);
        $view = new Zend_View();
        $view->addScriptPath(APPLICATION_PATH.'modules/admin/views/partials/');

        $view->assign('imgEditorImages', $this->getImages());
        $view->assign('imgEditorRemoveScript', $this->getRemoveScript());
        $view->assign('imgEditorSortScript', $this->getSortScript());
        $view->assign('imgEditorItemId', $this->getItemId());

        echo $view->render($this->partial);

    }


    public function setImages($images)
    {
        $this->images = $images;
    }

    public function getImages()
    {
        return $this->images;
    }

    public function setRemoveScript($removeScript)
    {
        $this->removeScript = $removeScript;
    }

    public function getRemoveScript()
    {
        return $this->removeScript;
    }

    public function setSortScript($sortScript)
    {
        $this->sortScript = $sortScript;
    }

    public function getSortScript()
    {
        return $this->sortScript;
    }

    public function setAllowRemoving($allowRemoving)
    {
        $this->allowRemoving = $allowRemoving;
    }

    public function getAllowRemoving()
    {
        return $this->allowRemoving;
    }

    public function setAllowSorting($allowSorting)
    {
        $this->allowSorting = $allowSorting;
    }

    public function getAllowSorting()
    {
        return $this->allowSorting;
    }

    public function setItemId($itemId)
    {
        $this->itemId = $itemId;
    }

    public function getItemId()
    {
        return $this->itemId;
    }



    private function prepareOptions($options)
    {

        if(isset($options['images'])) {
            $this->setImages($options['images']);
        }

        if(isset($options['itemId'])) {
            $this->setItemId($options['itemId']);
        }

        if(isset($options['sortScript'])) {
            $this->setSortScript($options['sortScript']);
        }else{
            $this->allowSorting(false);
        }

        if(isset($options['removeScript'])) {
            $this->setRemoveScript($options['removeScript']);
        }else{
            $this->setAllowRemoving(false);
        }

    }

    private function buildEditorView(){

    }



}