<?php

/**
 * {0}
 * 
 * @author
 * @version 
 */
	

class ErrorController extends Zend_Controller_Action
{
	/**
	 * The default action - show the home page
	 */
    public function errorAction() 
    {

        _logger()->warn('Default ErrorController');

        $defaultError = new stdClass();
        $defaultError->type = 'EXCEPTION_OTHER';
        $defaultError->exception = new Exception('Error Handler - Unknown Error', -1);

		$error = $this->_getParam('error_handler', $defaultError);



        switch ($error->type) {
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_CONTROLLER:
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_ACTION:
                // 404 Fehler -- Controller oder Aktion nicht gefunden
                $this->getResponse()
                     ->setRawHeader('HTTP/1.1 404 Not Found');

                 $this->view->assign('message', $this->view->translate('404 - Page not found'));

                break;

            default:
                // Anwendungsfehler; Fehler Seite anzeigen, aber den
                // Status Code nicht �ndern

                if(isset($error->noAuth) && $error->noAuth){
                    $this->_redirect('/admin/auth/login');
                }else{
                    $this->view->assign('message', $error->exception->getMessage());
                }

                break;
       
    	}


        if(APPLICATION_ENV != 'production'){
            $this->view->assign('error', $error);
        }
    }
    
}

