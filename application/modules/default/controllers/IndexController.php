<?php

/**
 * Index Controller
 * 
 */
class IndexController extends Zend_Controller_Action
{
		
	public function indexAction()
	{
           _logger()->info("Default Index-Action");
           $this->_redirect($this->getHelper('CustomUrl')->custom('index', 'index', 'admin'));
	}

    public function fileAction()
    {
        $file = $this->_request->getParam('file', false);
        $id = $this->_request->getParam('id', false);
        $type = $this->_request->getParam('type', false);

        if ($id === false || $type === false) {
            throw new Zend_Controller_Action_Exception('File not found', 404);
            return;
        }


        $objType = null;
        switch ($type) {
            case 'news':
                $objType = 'Epr_News';
                break;
            case 'image':
            case 'contentImage':
                $objType = 'Epr_Content_Image';
                break;
            case 'deal':
                $objType = 'Epr_Deal';
                break;
            case 'competition':
                $objType = 'Epr_Idea_Competition';
                break;
            case 'competitionCategory':
                $objType = 'Epr_Idea_Category';
                break;
            case 'idea':
                $objType = 'Epr_Idea';
                break;
        }

        $object = _dm()->getRepository($objType)->find($id);
        if (method_exists($object, 'getAttachments')) {
            $attachments = $object->getAttachments();
            
            if ($file === false) {
            	$file = "0";
            }
            
            $fileAsNumber = (int)$file;
            if ((string)$fileAsNumber === $file) {
            	if (is_integer($fileAsNumber)) {
            		if ($file < count($attachments)) {
						$keys = array_keys($attachments);
						$file = $keys[$file];            		
            		}
            	}
            }
            

            if (array_key_exists($file, $attachments)) {

                $item = $attachments[$file];
                /* @var Doctrine\CouchDB\Attachment $item */
                $this->getResponse()
                    ->setHeader('Content-type', $item->getContentType())
                    ->setHeader('Content-length', $item->getLength())
                    ->setHeader('Cache-Control', 'max-age=3600, must-revalidate', true)
                    ->setHeader('Content-Transfer-Encoding', 'binary', true)
                    ->setBody($item->getRawData());

                // display images -- download files
                if (!Epr_System::isImage($item->getContentType())) {
                     $this->getResponse()->setHeader('Content-Disposition', 'attachment; filename="'.$file.'"');
                }



                $this->getResponse()->sendResponse();
                exit;
            }
        }


    }
}
