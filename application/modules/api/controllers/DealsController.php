<?php

class Api_DealsController extends \Epr_Controller_ApiController {

	public function init() {
		parent::init();
		$this->registerContextForAction('index');	
	}
	
	public function indexAction() {
		
		$deals = Epr_Deal_Collection::getAll();
		$this->view->assign('deals',$deals);
		
	}

}