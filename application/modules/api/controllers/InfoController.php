<?php

class Api_InfoController extends Epr_Controller_ApiController {
	
	public function init() {
		parent::init();

		$this->registerContextForAction('index');
	}
	
	public function indexAction() {
		
		$this->view->assign('version',Epr_Application::application()->getVersion());
		
	}
	
}
