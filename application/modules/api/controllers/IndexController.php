<?php

class Api_IndexController extends \Epr_Controller_ApiController {

	public function indexAction() {
		$this->getHelper('viewRenderer')->setNoRender();
		$this->_redirect($this->getHelper('CustomUrl')->custom('index','info','api'));
	}

}