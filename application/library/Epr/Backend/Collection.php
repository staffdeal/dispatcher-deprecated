<?php

class Epr_Backend_Collection extends \Doctrine\ODM\CouchDB\DocumentRepository {

	/**
	 * @return \Doctrine\ODM\CouchDB\DocumentRepository
	 */
	static function getRepository() {
		return _dm()->getRepository('Epr_Backend');
	}
	
	static function getAllBackends() {
		$repo = self::getRepository();
		$backends = $repo->findAll();
		return backends;
	}
	
}

?>