<?php
/**
 * meinERP
 * User: Jan Fanslau
 * Date: 23.09.12
 * Time: 17:34
 */

use Doctrine\ODM\CouchDB\DocumentRepository;
use Doctrine\CouchDB\View\FolderDesignDocument;

/** @Document (repositoryClass="Epr_Content_Image_Collection") */
class Epr_Content_Image extends Epr_Document
{
    /** @Id */
    private $id;

    /** @Field(type="string") */
    public $name;

    /** @Field(type="string") */
    public $copyright;

    /** @Field(type="integer") */
    public $position;

    /** @Attachments */
    public $attachments;

    public function setAttachments($attachments)
    {
        $this->attachments = $attachments;
    }

    public function getAttachments()
    {
        return $this->attachments;
    }

    public function setCopyright($copyright)
    {
        $this->copyright = $copyright;
    }

    public function getCopyright()
    {
        return $this->copyright;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setPosition($position)
    {
        $this->position = $position;
    }

    public function getPosition()
    {
        return $this->position;
    }

}
