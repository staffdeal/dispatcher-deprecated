<?php
/**
 * meinERP
 * User: Jan Fanslau
 * Date: 08.09.12
 * Time: 12:56
 */
use Doctrine\ODM\CouchDB\DocumentRepository;
use Doctrine\CouchDB\View\FolderDesignDocument;

class Epr_Deal_Collection extends \Doctrine\ODM\CouchDB\DocumentRepository
{

    /**
     * @return \Doctrine\ODM\CouchDB\DocumentRepository
     */
    static function getRepository() {
        return _dm()->getRepository(Epr_Deal::DOCUMENTNAME);
    }

    static function getAll()
    {
        $coll = self::getRepository();
        return parray($coll->findAll());
    }
}