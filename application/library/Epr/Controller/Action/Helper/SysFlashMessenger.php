<?php
/**
 * Messenger stores System messages and outputs them when called
 * User: janfanslau
 * Date: 06.09.12
 * Time: 22:24
 */

class Epr_Controller_Action_Helper_SysFlashMessenger extends Zend_Controller_Action_Helper_FlashMessenger
{

    private $_static = false;
    public function setStatic(){
        $this->_static = true;
        return $this;
    }

    public function success($message)
    {
        if($this->_static) {
            $this->setNamespace('stat_success')->addMessage($message);
        } else {
            $this->setNamespace('success')->addMessage($message);
        }

    }

    public function error($message)
    {

        if($this->_static) {
            $this->setNamespace('stat_error')->addMessage($message);
        } else {
            $this->setNamespace('error')->addMessage($message);
        }
    }

    public function info($message)
    {
        if($this->_static) {
            $this->setNamespace('stat_info')->addMessage($message);
        } else {
            $this->setNamespace('info')->addMessage($message);
        }
    }

    public function notice($message)
    {

        if($this->_static) {
            $this->setNamespace('stat_notice')->addMessage($message);
        } else {
            $this->setNamespace('notice')->addMessage($message);
        }
    }



}