<?php

class Epr_Controller_Plugin_ErrorHandler extends Zend_Controller_Plugin_ErrorHandler {
	
	private $_didHandleError = false;
	
	/* (non-PHPdoc)
	 * @see Zend_Controller_Plugin_ErrorHandler::_handleError()
	 */protected function _handleError(Zend_Controller_Request_Abstract $request) {
			parent::_handleError($request);
			
			if ($this->_didHandleError === false) {
				
				$error = $request->getParam('error_handler',null);
				if ($error && $error instanceof ArrayObject) {
				
					$config = Zend_Registry::get('config');
					$airbrake = $config['airbrake'];
					if ($airbrake) {
						$apikey = $airbrake['apikey'];
						if ($apikey && is_string($apikey) && strlen($apikey) > 10) {
							$enabled = $airbrake['enable'];
							if ($enabled == '1' || $enabled == 'true' || $enabled == 'yes' || $enabled == 'on') {
								$options = array(
									'apiEndPoint' => 'https://exceptions.codebasehq.com/notifier_api/v2/notices'
								);
							
								$airbrakeConfig = new Airbrake\Configuration($apikey, $options);
								$client = new Airbrake\Client($airbrakeConfig);
							
								$exception = $error['exception'];
								$client->notifyOnException($exception);
								
								$this->_didHandleError = true;
							
							}
						}
					}
				}
			}
			
		}

}

?>