<?php

class Epr_Controller_ApiController extends Zend_Controller_Action {
	

    public function preDispatch()
    {
        if ($this->_request->getParam('format','xml') == 'json') {
            $this->_helper->layout()->setLayout('api-json');
        } else {
            $this->_helper->layout()->setLayout('api-xml');
        }
    }
    
    public function postDispatch() {
    	$this->getResponse()->setHeader('X-Api-Version', Epr_Application::application()->getVersion());
	}
	
	protected function registerContextForAction($action) {
		$contextSwitch = $this->_helper->getHelper('contextSwitch');
		$contextSwitch->addActionContext($action, 'xml')
		->addActionContext($action, 'json')
		->setAutoJsonSerialization(false)
		->initContext($this->getRequest()
		->getParam('format', 'xml'));
	}
	
}
