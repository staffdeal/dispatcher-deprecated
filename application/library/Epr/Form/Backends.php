<?php

use Poundation\PURL;

class Epr_Form_Backends extends Epr_Form {

	static function URLForm($url = false) {
		
		$form = new self();
		$form->setEnctype('multipart/form-data');
		
		$url = new Zend_Form_Element_Text('url');
		$url->setLabel('URL');
		$url->setRequired(true);
		$url->addValidator(new Epr_Validate_URL());
		$url->setAttrib('class','inp-form');
		$url->setDecorators(array('Composite'));
		
		if (is_string($url) || $url instanceof PURL) {
			if (is_string($url)) {
				$url = new PURL($url);
			}	
			$url->setValue($url);
		}
		
		$submit = new Zend_Form_Element_Submit('submit');
		$submit->setLabel('');
		$submit->setValue('Register Backend');
		$submit->setAttrib('class','form-submit');
		$submit->setDecorators(array('Composite'));

		$form->addElements(array($url, $submit));
		
		return $form;

	}
	
}

?>