<?php
/**
 * meinERP
 * User: Jan Fanslau
 * Date: 07.09.12
 * Time: 21:35
 */
class Epr_Form_Deal extends Epr_Form
{

    static function dealForm($deal = null, $categories = null, $step = null){

        $form = new self();
        $form->setEnctype('multipart/form-data');

        $title = new Zend_Form_Element_Text('title');
        $title->setLabel('Title');
        $title->setRequired(true);
        $title->setAttrib('class','inp-form');
        $title->setDecorators(array('Composite'));

        $startDate = new Zend_Form_Element_Text('startdate');
        $startDate->setLabel('Start Date');
        $startDate->setRequired(true);
        $startDate->setAttrib('class','inp-form date-picker');
        $startDate->setAttrib('id','startdate');
        $startDate->setDecorators(array('Composite'));

        $endDate = new Zend_Form_Element_Text('enddate');
        $endDate->setLabel('End Date');
        $endDate->setRequired(true);
        $endDate->setAttrib('class','inp-form date-picker');
        $endDate->setAttrib('id','enddate');

        $endDate->setDecorators(array('Composite'));
        
        $teaser = new Zend_Form_Element_Textarea('teaser');
        $teaser->setLabel('Teaser Text')
        	->setRequired(false)
        	->setDecorators(array('Composite'));

        $text = new Zend_Form_Element_Textarea('text');
        $text->setRequired(true);
        //$text->setAttrib('id','wysiwyg');
        $text->setAttrib('class', 'wysiwyg');
        $text->setDecorators(array('Composite'));
        $text->setLabel('Text');

        $image = new Zend_Form_Element_File('titleImage');
        $image->setDecorators(array(
            'File',
            'Errors',
            array(array('data' => 'HtmlTag'), array('tag' => 'td')),
            array('Label', array('tag' => 'th')),
            array(array('row' => 'HtmlTag'), array('tag' => 'tr'))
        ));
        $image->setLabel('Title Image');

        $isFeatured = new Zend_Form_Element_Checkbox('isFeatured');
        $isFeatured->setDecorators(array('Composite'));
        $isFeatured->setLabel('Is Featured');

        $isActive = new Zend_Form_Element_Checkbox('isActive');
        $isActive->setDecorators(array('Composite'));
        $isActive->setLabel('Is Active');


        $form->addElements(array($title, $startDate, $endDate, $image));

        /** @var $deal Epr_Deal */
        if($deal instanceof Epr_Deal){
            $title->setValue($deal->getTitle());
            $startDate->setValue($deal->getStartDate()->format('Y-m-d'));
            $endDate->setValue($deal->getEndDate()->format('Y-m-d'));
            $teaser->setValue($deal->getTeaser());
            $text->setValue($deal->getText());
            $isFeatured->setValue($deal->isFeatured());
            $isActive->setValue($deal->isActive());

            $hiddenId = new Zend_Form_Element_Hidden('id');
            $hiddenId->setValue($deal->getId());

            $form->addElement($hiddenId);
            if (isset($deal->attachments['image'])) {
                $checkImage = new Zend_Form_Element_Image('check');
                $checkImage->setImageValue('data:' . $deal->attachments['image']->getContentType() . ';base64,' . $deal->attachments['image']->getBase64EncodedData() );
                $checkImage->setAttrib('disabled', true);
                $checkImage->setAttrib('width', '200');
                $checkImage->setLabel('Current Image');
                $checkImage->setDecorators(array(
                    'Image',
                    'Errors',
                    array(array('data' => 'HtmlTag'), array('tag' => 'td')),
                    array('Label', array('tag' => 'th')),
                    array(array('row' => 'HtmlTag'), array('tag' => 'tr'))
                ));
                $removeImage = new Zend_Form_Element_Checkbox('removeImage');
                $removeImage->setDecorators(array('Composite'));
                $removeImage->setLabel('Remove Image');
                $form->addElements(array($checkImage, $removeImage));
            }

        }


        $submit = new Zend_Form_Element_Submit('submit');
        $submit->setLabel('');
        $submit->setValue('Submit');
        $submit->setAttrib('class','form-submit');
        $submit->setDecorators(array('Composite'));
        $reset = new Zend_Form_Element_Submit('reset');
        $reset->setValue('Reset');
        $reset->setLabel('');
        $reset->setAttrib('class','form-reset');
        $reset->setDecorators(array('Composite'));

        $form->addElements(array($teaser,$text, $isFeatured, $isActive, $submit, $reset));

        return $form;
    }
}
