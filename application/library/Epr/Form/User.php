<?php
/**
 * meinERP
 * User: Jan Fanslau
 * Date: 07.09.12
 * Time: 21:35
 */
class Epr_Form_User extends Epr_Form
{



    static function userForm($user = null, $step = null){

        $form = new self();

        $firstname = new Zend_Form_Element_Text('lastname');
        $firstname->setLabel('Lastname');
        $firstname->setRequired(true);
        $firstname->setAttrib('class','inp-form');
        $firstname->setDecorators(array('Composite'));

        $email = new Zend_Form_Element_Text('email');
        $email->setLabel('E-Mail');
        $email->setRequired(true);
        $email->setAttrib('class','inp-form');
        $email->setDecorators(array('Composite'));
        $email->setValidators(array('EmailAddress'));

        $lastname = new Zend_Form_Element_Text('firstname');
        $lastname->setLabel('Firstname');
        $lastname->setRequired(true);
        $lastname->setAttrib('class','inp-form');
        $lastname->setDecorators(array('Composite'));

        $password = new Zend_Form_Element_Password('password');
        $password->setLabel('Password');
        $password->setAttrib('class','inp-form');
        $password->setDecorators(array('Composite'));

        $passwordRepeat = new Zend_Form_Element_Password('password_repeat');
        $passwordRepeat->setLabel('Password Repeat');
        $passwordRepeat->setAttrib('class','inp-form');
        $passwordRepeat->setDecorators(array('Composite'));

        /**
         * @todo Dynamic field generation
         */


        $role = new Zend_Form_Element_Select('role');
        
        $roles = Epr_Roles::allRoles();
        $roles->removeValueForKey(Epr_Roles::ROLE_GUEST);
        
        $role->setMultiOptions($roles->nativeArray());
        $role->setAttrib('class','styledselect_form_1');
        $role->setDecorators(array('Composite'));
        $role->setLabel('Role');

        $isActive = new Zend_Form_Element_Checkbox('isActive');
        $isActive->setDecorators(array('Composite'));
        $isActive->setLabel('Is Active');


        /** @var $user Epr_User */
        if($user instanceof Epr_User){
            $firstname->setValue($user->getFirstname());
            $lastname->setValue($user->getLastname());
            $email->setValue($user->getEmail());
            $role->setValue($user->getRole());
            $isActive->setValue($user->isActive());

            $hiddenId = new Zend_Form_Element_Hidden('id');
            $hiddenId->setValue($user->getId());
            $form->addElement($hiddenId);

        }


        $submit = new Zend_Form_Element_Submit('submit');
        $submit->setLabel('');
        $submit->setValue('Submit');
        $submit->setAttrib('class','form-submit');
        $submit->setDecorators(array('Composite'));
        $reset = new Zend_Form_Element_Submit('reset');
        $reset->setValue('Reset');
        $reset->setLabel('');
        $reset->setAttrib('class','form-reset');
        $reset->setDecorators(array('Composite'));

        $form->addElements(array($firstname, $lastname, $email, $password, $passwordRepeat, $role, $isActive, $submit, $reset));

        return $form;
    }


    static function getLoginForm()
    {
        $form = new self();

        $email = new Zend_Form_Element_Text('email');

        $email->setRequired(true);
        $email->setAttrib('class','login-inp');
        $email->setLabel('');
        $email->setValidators(array('EmailAddress'));
        $email->removeDecorator('label')
            ->removeDecorator('HtmlTag');

        $password = new Zend_Form_Element_Password('password');
        $password->setRequired(true);
        $password->setLabel('');
        $password->setAttrib('class','login-inp');
        $password->removeDecorator('label')
                 ->removeDecorator('HtmlTag');

        $form->addElements(array($email, $password));


        return $form;
    }

    static function getForgetPasswordForm()
    {
        $form = new self();

        $email = new Zend_Form_Element_Text('email');
        $email->setLabel('E-Mail');
        $email->setRequired(true);
        $email->setAttrib('class','inp-form');
        $email->setDecorators(array('Composite'));
        $email->setValidators(array('EmailAddress'));

        $form->addElements(array($email));

        return $form;
    }
}
