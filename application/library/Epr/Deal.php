<?php
/**
 * meinERP
 * User: Jan Fanslau
 * Date: 14.09.12
 * Time: 12:26
 */
/** @Document (repositoryClass="Epr_Deal_Collection") */
class Epr_Deal extends Epr_Document
{
    const DOCUMENTNAME = 'Epr_Deal';
    /** @Id */
    private $id;

    /** @Field(type="string") */
    private $title;

    /** @Field(type="string") */
    private $teaser;
    
    /** @Field(type="string") */
    private $text;

    /** @Field(type="datetime") */
    private $startDate;

    /** @Field(type="datetime") */
    private $endDate;

    /** @Field(type="string") */
    private $image;

    /** @Field(type="boolean") */
    private $featured;

    /**
     * @var boolean
     * @Field(type="boolean")
     */
    private $active;

    /** @Attachments */
    public $attachments;

    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;
    }

    public function getEndDate()
    {
        return $this->endDate;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setImage($image)
    {
        $this->image = $image;
    }

    public function getImage()
    {
        return $this->image;
    }

    public function setTitle($title)
    {
        $this->title = $title;
    }

    public function getTitle()
    {
        return $this->title;
    }
    
    public function setTeaser($teaser)
    {
    	$this->teaser = $teaser;	
    }
    
    public function getTeaser()
    {
    	return $this->teaser;
    }

    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;
    }

    /**
     * @return DateTime
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    public function setText($text)
    {
        $this->text = $text;
    }

    public function getText()
    {
        return $this->text;
    }

    public function setAsFeatured($featured)
    {
        $this->featured = $featured;
    }

    public function isFeatured()
    {
        return $this->featured;
    }

    public function setAttachments($attachments)
    {
        $this->attachments = $attachments;
    }

    public function getAttachments()
    {
        return $this->attachments;
    }

    /**
     * @param boolean $active
     */
    public function setActive($active)
    {
        $this->active = $active;
    }

    /**
     * @return boolean
     */
    public function isActive()
    {
        return $this->active;
    }






}

