<?php 

final class Epr_Roles extends Epr_Roles_Abstract {
	
	const ROLE_NONE = 'none';
	const ROLE_GUEST = 'guest';
	const ROLE_ADMIN = 'administrator';
	const ROLE_ROOT = 'root';
	
}
