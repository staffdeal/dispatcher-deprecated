<?php

/** @Document (repositoryClass="Epr_Backend_Collection") */
class Epr_Backend extends Epr_Document {

	/** @Id */
	private $id;
	
	/** @Field(type="string") */
	private $url;
	
	/** @Field(type="string") */
	private $name;
	
	/** @Field(type="datetime") */
	private $created;
	
	/** @Field(type="datetime") */
	private $lastContact;

    public function __construct($url, $name = false) {
        $checkedURL = \Poundation\PURL::

    }

    /**
     * Returns the id of the backend. This is only valid in the scope if this system.
     * @return string
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Returns the URL of the backend.
     * @return string
     */
    public function getUrl() {
        return $this->url;
    }

    /**
     * Returns the datetime of creation.
     * @return Datetime
     */
    public function getCreated() {
        return $this->created;
    }

    /**
     * Returns the name of the backend.
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Sets the name of the backend.
     * @param string $name
     * @return Epr_Backend
     */
    public function setName($name) {
        $this->name = $name;
        return $this;
    }

    /**
     * Returns the datetime of the last contact to the backend.
     * @return Datetime
     */
    public function getLastContact() {
        return $this->lastContact;
    }

    /**
     * Sets the datetime of the last contact to now.
     * @return Epr_Backend
     */
    public function refreshLastContact() {
        $this->lastContact = new DateTime();
        return $this;
    }

}

?>