<?php 
class Plugins_Auth_Acl extends Zend_Acl
{

    protected static $_instance;

    protected function __construct()
    {
        // ROLES
        $this->addRole(new Zend_Acl_Role(Epr_Roles::ROLE_GUEST));
        $this->addRole(new Zend_Acl_Role(Epr_Roles::ROLE_ADMIN), Epr_Roles::ROLE_GUEST);
        $this->addRole(new Zend_Acl_Role(Epr_Roles::ROLE_ROOT), Epr_Roles::ROLE_ADMIN);


        // RESOURCES
        $this->addResource(new Zend_Acl_Resource('mvc:admin'));
        $this->addResource(new Zend_Acl_Resource('mvc:admin.index', 'mvc:admin'));
        $this->addResource(new Zend_Acl_Resource('mvc:admin.auth', 'mvc:admin'));
        $this->addResource(new Zend_Acl_Resource('mvc:admin.user', 'mvc:admin'));
        $this->addResource(new Zend_Acl_Resource('mvc:admin.deals', 'mvc:admin'));
        $this->addResource(new Zend_Acl_Resource('mvc:admin.settings', 'mvc:admin'));

        $this->allow(null, 'mvc:admin');
        $this->allow(Epr_Roles::ROLE_GUEST, 'mvc:admin.auth', array('login'));
        $this->allow(Epr_Roles::ROLE_ADMIN, 'mvc:admin.auth', array('logout'));
        $this->allow(Epr_Roles::ROLE_ADMIN, 'mvc:admin.index');
        $this->allow(Epr_Roles::ROLE_ADMIN, 'mvc:admin.user');
        $this->allow(Epr_Roles::ROLE_ADMIN, 'mvc:admin.deals');
        $this->allow(Epr_Roles::ROLE_ADMIN, 'mvc:admin.settings');

        $this->addResource(new Zend_Acl_Resource('mvc:api'));
        $this->addResource(new Zend_Acl_Resource('mvc:api.error'), 'mvc:api');
        $this->addResource(new Zend_Acl_Resource('mvc:api.index'), 'mvc:api');
        $this->addResource(new Zend_Acl_Resource('mvc:api.auth'), 'mvc:api');
        $this->addResource(new Zend_Acl_Resource('mvc:api.info'), 'mvc:api');
        $this->addResource(new Zend_Acl_Resource('mvc:api.deals'), 'mvc:api');
        $this->addResource(new Zend_Acl_Resource('mvc:api.com'), 'mvc:api');

        $this->allow(Epr_Roles::ROLE_GUEST, 'mvc:api.error');
        $this->allow(Epr_Roles::ROLE_GUEST, 'mvc:api.index');
        $this->allow(Epr_Roles::ROLE_GUEST, 'mvc:api.auth');
        $this->allow(Epr_Roles::ROLE_GUEST, 'mvc:api.info');
        $this->allow(Epr_Roles::ROLE_GUEST, 'mvc:api.deals');
        $this->allow(Epr_Roles::ROLE_GUEST, 'mvc:api.com');

        $this->addResource(new Zend_Acl_Resource('mvc:default'));
        $this->allow(Epr_Roles::ROLE_GUEST, 'mvc:default');

        return $this;

    }

    /**
     *
     * @return Plugins_Auth_Acl
     */
    public static function getInstance()
    {
        if (null === self::$_instance) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public static function resetInstance()
    {
        self::$_instance = null;
        self::getInstance();
    }
    
}