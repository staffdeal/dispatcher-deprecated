function (doc) {
    if (doc.type == 'Epr_News' && doc.active) {
    	var date=doc.publishDate.split(" ");
        emit(date[0], doc);
    }
}