function (doc) {
    if (doc.type == 'Epr_User' && doc.active) {
        emit(doc.email, doc.password);
    }
}