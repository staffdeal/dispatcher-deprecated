<?php

$couchPath = "/var/www/meinepr/library";
require_once $couchPath . "/Doctrine/Common/ClassLoader.php";

$loader = new \Doctrine\Common\ClassLoader('Doctrine\Common', $couchPath);
$loader->register();

$loader = new \Doctrine\Common\ClassLoader('Doctrine\ODM\CouchDB', $couchPath);
$loader->register();

$loader = new \Doctrine\Common\ClassLoader('Doctrine\CouchDB', $couchPath);
$loader->register();

$loader = new \Doctrine\Common\ClassLoader('Symfony', $couchPath);
$loader->register();

$annotationNs = 'Doctrine\\ODM\\CouchDB\\Mapping\\Annotations';
Doctrine\Common\Annotations\AnnotationRegistry::registerAutoloadNamespace($annotationNs, $couchPath);


?>